package com.example.eurekaclient.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequiredArgsConstructor
public class EurekaController {

    private final RestTemplate restTemplate;
    @Value("${eureka.instance.instance-id}")
    private String id;



    @GetMapping("/test")
    public String test() {
        return "Instance " + id;
    }
}
