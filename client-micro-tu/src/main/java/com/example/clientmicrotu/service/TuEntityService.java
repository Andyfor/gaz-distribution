package com.example.clientmicrotu.service;

import com.example.clientmicrotu.entity.ClientEntity;
import com.example.clientmicrotu.entity.TuEntity;
import com.example.clientmicrotu.repository.ClientEntityServiceRepository;
import com.example.clientmicrotu.repository.TuEntityServiceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TuEntityService {

    private final TuEntityServiceRepository tuEntityServiceRepository;

    public void save(TuEntity tu) {
        tuEntityServiceRepository.save(tu);
    }
}
