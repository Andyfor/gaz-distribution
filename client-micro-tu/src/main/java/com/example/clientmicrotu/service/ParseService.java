package com.example.clientmicrotu.service;

import com.example.clientmicrotu.entity.ClientEntity;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Service
@RequiredArgsConstructor
public class ParseService {

    private static final Logger logger = Logger.getLogger(ParseService.class.getName());

    private final ClientEntityService clientEntityService;

    public void parse(InputStream inputStream) {
        try (Workbook workbook = WorkbookFactory.create(inputStream)) {
            Sheet sheet = workbook.getSheetAt(0);
            loopSheet(sheet);
        } catch (IOException e) {
            throw new RuntimeException("cannot read data. ", e);
        }
    }

    /**
     * обработка листа формата "STRING", "STRING", "STRING", "STRING", "NUMERIC"
     * @param sheet List excel файла
     * @return
     */
    private void loopSheet(Sheet sheet) {
        Map<Integer, List<String>> rowsMap = new HashMap<>();

        int i = 0;
        for (Row row : sheet) {
            if (i == 0) {
                i++;
                continue;
            }

            rowsMap.put(i, new ArrayList<>());
            List<String> typeData = List.of("STRING", "STRING", "STRING", "STRING", "NUMERIC");

            for (int index = 0; index < typeData.size(); index++) {
                Cell cell = row.getCell(index);
                if (typeData.get(index).equals(cell.getCellType().toString())) {
                    switch (cell.getCellType()) {
                        case STRING -> rowsMap.get(i).add(cell.getStringCellValue());
                        case NUMERIC -> rowsMap.get(i).add(String.valueOf(cell.getNumericCellValue()));
                        default -> rowsMap.get(i).add("no data");
                    }
                } else {
                    throw new RuntimeException("incorrect data in cell");
                }
            }
            i++;
        }
        saveToDb(rowsMap);
    }

    // proxy repository list entity, batch
    @Transactional
    public void saveToDb(Map<Integer, List<String>> rowsMap) {
        rowsMap.forEach((k, v) -> {
            ClientEntity client = new ClientEntity();
            client.setName(v.get(0));
            client.setSurname(v.get(1));
            client.setMiddlename(v.get(2));
            client.setAddressClient(v.get(3));
            client.setClientCredit(new BigDecimal(v.get(4)));
            clientEntityService.save(client);
        });
    }
}



//                for (Cell cell: row) {
//                    switch (cell.getCellType()) {
//                        case STRING -> rowsMap.get(i).add(cell.getStringCellValue());
//                        case NUMERIC -> rowsMap.get(i).add(String.valueOf(cell.getNumericCellValue()));
//                        default -> rowsMap.get(i).add("no data");
//                }