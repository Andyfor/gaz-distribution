package com.example.clientmicrotu.service;

import com.example.clientmicrotu.entity.ClientEntity;
import com.example.clientmicrotu.repository.ClientEntityServiceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ClientEntityService {

    private final ClientEntityServiceRepository clientEntityServiceRepository;

    public void save(ClientEntity client) {
        clientEntityServiceRepository.save(client);
//        clientEntityServiceRepository.s
    }
}
