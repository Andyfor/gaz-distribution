package com.example.clientmicrotu.controller;

import com.example.clientmicrotu.entity.ClientEntity;
import com.example.clientmicrotu.entity.TuEntity;
import com.example.clientmicrotu.service.ClientEntityService;
import com.example.clientmicrotu.service.TuEntityService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/tu")
public class TuController {

    private final TuEntityService tuEntityService;

    @PostMapping("/")
    public ResponseEntity<TuEntity> save(@RequestBody TuEntity tu) {
        if (tu == null) {
            return new ResponseEntity("no client data", HttpStatus.NOT_ACCEPTABLE);
        }
        tuEntityService.save(tu);
//        if (client.getId() != null || client.getId() != 0) {
//            return new ResponseEntity("id must be empty", HttpStatus.NOT_ACCEPTABLE);
//        }
//        if (client.getTitle() == null || client.getTitle().trim().length() == 0) {
//            return new ResponseEntity("title cannot be empty", HttpStatus.NOT_ACCEPTABLE);
//        }

        return ResponseEntity.ok(tu);
    }
}
