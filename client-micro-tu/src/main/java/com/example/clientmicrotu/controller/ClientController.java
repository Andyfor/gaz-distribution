package com.example.clientmicrotu.controller;

import com.example.clientmicrotu.entity.ClientEntity;
import com.example.clientmicrotu.service.ClientEntityService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/client")
public class ClientController {

    private final ClientEntityService clientEntityService;

    @GetMapping("/test")
    public String getTest() {
        return "Success";
    }

    @GetMapping("/client-page")
    public String getClientPage() {
        return "client-page.html";
    }

//    @GetMapping("/index")
//    public String getIndex() {
//        return "index";
//    }

    @PostMapping("/")
    public ResponseEntity<ClientEntity> save(@RequestBody ClientEntity client) {
        if (client == null) {
            return new ResponseEntity("no client data", HttpStatus.NOT_ACCEPTABLE);
        }
        clientEntityService.save(client);
//        if (client.getId() != null || client.getId() != 0) {
//            return new ResponseEntity("id must be empty", HttpStatus.NOT_ACCEPTABLE);
//        }
//        if (client.getTitle() == null || client.getTitle().trim().length() == 0) {
//            return new ResponseEntity("title cannot be empty", HttpStatus.NOT_ACCEPTABLE);
//        }

        return ResponseEntity.ok(client);
    }
}
