package com.example.clientmicrotu.controller;

import com.example.clientmicrotu.service.ParseService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Controller
@RequiredArgsConstructor
public class UploadController {

    private final ParseService parseService;

    @PostMapping("/upload")
    public void upload(@RequestParam("file") MultipartFile file) throws IOException {
        // check NPE move to service

        if (file.getSize() > 10 * 1024 * 1024) {
            throw new RuntimeException("file is too large");
        } else if (!file.isEmpty()) {
            System.out.println(file);
            parseService.parse(file.getInputStream());
        } else {
            throw new RuntimeException("file is empty");
        }


    }
}
