package com.example.clientmicrotu.entity;

public enum TuStatus {
    CREATED, ALLOWED, DENIED, ISSUED, PROJECT, PROJECT_AGREED, REALIZED
}
