package com.example.clientmicrotu.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TuEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    private String tuNumber;

    private LocalDate date;

    @Enumerated(value = EnumType.STRING)
    private TuStatus tuStatus;

    private String condition;

    private String addressGaz;

    private BigDecimal tuPrice;

    @ManyToOne
    private ClientEntity client;
}
