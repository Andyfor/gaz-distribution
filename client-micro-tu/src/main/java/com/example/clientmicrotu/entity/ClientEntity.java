package com.example.clientmicrotu.entity;

import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ClientEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    private String name;

    private String surname;

    private String middlename;

    private String addressClient;

    private BigDecimal clientCredit;

    @OneToMany(mappedBy = "client")
    private List<TuEntity> tuEntities;

    // getter
    @NonNull
    public List<TuEntity> getTuEntities() {
        if (tuEntities != null) {
            return tuEntities;
        }
        tuEntities = new ArrayList<>();
        return tuEntities;
    }

}
