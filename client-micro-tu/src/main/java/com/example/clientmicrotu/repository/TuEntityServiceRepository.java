package com.example.clientmicrotu.repository;

import com.example.clientmicrotu.entity.ClientEntity;
import com.example.clientmicrotu.entity.TuEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface TuEntityServiceRepository extends JpaRepository<TuEntity, UUID> {
}
