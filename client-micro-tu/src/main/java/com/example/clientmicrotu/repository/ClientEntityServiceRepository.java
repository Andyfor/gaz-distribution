package com.example.clientmicrotu.repository;

import com.example.clientmicrotu.entity.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ClientEntityServiceRepository extends JpaRepository<ClientEntity, UUID> {
}
