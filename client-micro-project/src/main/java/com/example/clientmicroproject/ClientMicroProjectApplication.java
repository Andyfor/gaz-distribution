package com.example.clientmicroproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableDiscoveryClient
//@ComponentScan(basePackages = {"com.example.clientmicrotu"})
//@EnableJpaRepositories(basePackages = {"com.example.clientmicrotu"})
public class ClientMicroProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClientMicroProjectApplication.class, args);
    }

}
