package com.example.clientmicroproject.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProjectAgreementEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

//    private Client client;

//    private TuEntity tuEntity;

    private BigDecimal projectPrice;

    @Enumerated(value = EnumType.STRING) // how by default
    private Payment paymentStatus;

    private String projectManager; // how to set ?

    private String boilerMark;

    private LocalDate projectStartDate;

    // Project document ?
}
